<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/page', function () {
    return view('page');
});

Route::get('page/A', function () {
    return view('A');
});

Route::get('page/B', function () {
    return view('B');
});

Route::get('page/C', function () {
    return view('C');
});

/*Route::get('/', ['uses' => 'PagesController@index']);

Route::get('pages', function () {
    $pages = [
		'0' => ['page' => 'A'],
		'1' => ['page' => 'B'],
		'2' => ['page' => 'C']
	];
	return $pages;
}); */
