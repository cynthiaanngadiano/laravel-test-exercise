<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function A(){
		return "A";
	}
	
	public function B(){
		return "B";
	}
	
	public function C(){
		return "C";
	}
}
